Arcvi Toolbox R Package
==========================

## Installing/Updating this package ##

First of all RESET R, then execute

    library(devtools)
    install_bitbucket('arcvi/arcvitoolbox')

The toolbox can be loaded as a standard package `library(arcvitoolbox)`

For the moment it provides the functions:
 
 - arcvi_descriptivos
  - read.excel
  - write.excel
  - dput

## How to build an R package ##
https://hilaryparker.com/2014/04/29/writing-an-r-package-from-scratch/

When we modify this package we should only execute `library(devtools)`, `document()` and after that `check()`

## demo/modelling.R ##
Cross validation framework and common modelling functions and comments about it.

## demo/descriptivos.R ##
Function to create "descriptivos" given a feature and the target variable.

## demo/parallel_computing.R ##
Examples of parallelization of tasks in R

## demo/feature_selection.R ##
Common functions for feature selection based on different criteria.

## demo/simplex.R ##
Common libraries for doing a simplex.

## demo/time_series.R ##
Functions and tools to deal with time series. (TBD)

## style.txt ##
Arcvi R Style Guide