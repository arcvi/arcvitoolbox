library(data.table)

#' Reads data from clipboard and generates a data.table (usually the table comes from Excel)
#'
#' @param   header: does the data include a header?
#'
#' @return   The data.table
#' @export
read.excel = function(header=TRUE, ...){
  if(Sys.info()['sysname']=='Windows'){ # for windows
    data.table(read.table("clipboard",sep="\t",header=header,stringsAsFactors = F,...))
  }
  else{ # for mac? better add Sys.info()['sysname']
    data.table(read.table(pipe("pbpaste"),sep="\t",header=header,stringsAsFactors = F,...))    
  }
}

#' Copies data from R to clipboard ready to be pasted on Excel
#'
#' @param   x: data.frame or data.table to be copied
#'
#'
#' @return   Does not return anything
#' @export

write.excel = function(x,row.names=FALSE,col.names=TRUE, ...){
  if(Sys.info()['sysname']=='Windows'){ # for windows
    write.table(x,"clipboard-16384",sep="\t",row.names=row.names,col.names=col.names,...) #this 16384 means that the limit is 16384KB of text for the clipboard
  }
  else{ # for mac? better add Sys.info()['sysname']
    clip <- pipe("pbcopy", "w")
    write.table(x, file=clip, sep = '\t',row.names=row.names,col.names=col.names,...)                   
    close(clip)
  }
}

# ######## EJEMPLO TESTING #####################
# 
# N=1000 #tabla grande ...
# 
# dt = data.table(x=runif(N),y=runif(N))
# write.excel(dt)
# 
# library(magrittr)
# dt %>% write.excel # yo uso bastante esta construccion
# 
# #modify the data in excel
# 
# tmp = read.excel() # test reading 